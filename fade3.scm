(use-modules (ice-9 rdelim)
	     (ice-9 match)
	     (ice-9 receive)
	     (srfi srfi-1)
             (ice-9 format))

(system "stty -F /dev/ttyACM0 cs8 115200 ignbrk -brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke noflsh -ixon -crtscts")

(set! *random-state* (random-state-from-platform))

(define out (open-file "/dev/ttyACM0" "w0+"))

(define pi 3.14159)

(define (deg->rad degrees)
  (* degrees (/ pi 180)))

(define (hsv->rgb h s v)
  (let* ((chroma (* v s))
	 (hue' (/ h (/ pi 3)))
	 (x (* chroma (- 1 (abs (- (euclidean-remainder hue' 2) 1)))))
	 (m (- v chroma)))
    (receive (r₁ g₁ b₁)
	(case (inexact->exact (floor hue'))
	  ((0) (values chroma x 0))
	  ((1) (values x chroma 0))
	  ((2) (values 0 chroma x))
	  ((3) (values 0 x chroma))
	  ((4) (values x 0 chroma))
	  ((5) (values chroma 0 x)))
      (values (+ r₁ m) (+ g₁ m) (+ b₁ m)))))

;; (define ts (make-termios-struct))
;; ;; Get the current setings of the serial-device.
;; (tc-get-attr! out ts)
;; (cf-set-speed! ts termios-B9600)
;; (tc-set-attr out ts)

(sleep 3)

(define tick-increment (/ (* 2 pi) (* 360 1)))

(define (random-ticks n) (* tick-increment (random n)))

(define last-pulse (car (gettimeofday)))

(let loop ((tick 0)
	   (saturation 0))
  (format #t "~A " saturation)
  (let ((h (euclidean-remainder tick (* 2 pi))))
    (receive (r g b)
	     (hsv->rgb h (or 0.9 (- 1.0 (* (min saturation 10) 0.1))) 1)
      (let ((r' (inexact->exact (round (* r 255))))
	    (g' (inexact->exact (round (* g 255))))
	    (b' (inexact->exact (round (* b 255)))))
	
#;
	(format #t "~A ~A ~A -> ~A ~A ~A~%" h 1 1 r' g' b')
	(format out "~A,~A,~A,0~c" r' g' b' #\return)
	(format out "~A,~A,~A,1~c" r' g' b' #\return)
	#;(display (read-line out))))
    (usleep 450000)
    (loop (+ tick-increment tick) 
	  (if (> (- (car (gettimeofday)) last-pulse) 1)
	      (begin (set! last-pulse (car (gettimeofday)))
		     8)
	      (if (= saturation 0) 0 (1- saturation))))))

#;
(let loop ((rt (random-ticks 128))
	   (bt (random-ticks 128))
	   (gt (random-ticks 128)))
  (let ((r (inexact->exact (truncate (* 128 (+ 1 (sin rt))))))
	(g (- 256 (inexact->exact (truncate (* 128 (+ 1 (cos gt)))))))
	(b (inexact->exact (truncate (* 128 (+ 1 (sin bt)))))))
    #;(format #t "~A ~A ~A~%" r g b)
    (format out "~A,~A,~A,4~c" r g b #\return)
    (display (read-line out))
    (usleep 45000)
    (loop (+ rt (* tick-increment (1+ (random 3))))
	  (+ gt (* tick-increment (1+ (random 3))))
	  (+ bt (* tick-increment (1+ (random 3)))))))

