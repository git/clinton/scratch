(mapcar #'ql:quickload '(:cl-fad :cl-ppcre))

(defun copy-season (source target)
  (fad:walk-directory source
		      (lambda (x) (multiple-value-bind (target-name matched?)
				 (ppcre:regex-replace ".*/([0-9]{2})[.]([0-9]{2})[.](?:[0-9]{2})- (.*)"
						      (namestring x)
						      '("Season " 0 " Episode " 1 " - " 2))
			       (when matched?
				 (format t "matched, copying: ~A / ~A~%" x target-name)
				 (fad:copy-file x (merge-pathnames target-name (make-pathname :directory `(:absolute ,target)))))))))

"/srv/archive/video/tv/The X-Files/Season 01"