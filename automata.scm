(use-modules (srfi srfi-9)
	     (srfi srfi-1))

(define (next-token stream)
  (if (pair? stream)
      (car stream)))

(define (transition machine state)
  ((cdr (assoc state (cdr machine)))
   (cons (if (pair? (car machine)) (cdr (car machine)))
	 (cdr machine))
   (next-token (car machine))))

(define (run-machine machine initial-state stream)
  (transition (cons stream machine) initial-state))

(define cadr-machine
  `((init . ,(lambda (m v) (case v
		       ((#\c) (transition m 'more))
		       (else #f))))
    (more . ,(lambda (m v) (case v
		       ((#\a #\d) (transition m 'more))
		       ((#\r) (transition m 'end))
		       (else #f))))
    (end . ,(lambda (m v) #t))))


