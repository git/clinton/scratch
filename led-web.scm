(use-modules (ice-9 match)

	     (web server)
	     (web request)
	     (web response)
	     (web uri))

(define (led-command-handler request body)
  (match (split-and-decode-uri-path (uri-path (request-uri request)))
    (("set" "rgb" r g b)
     (set-led-color/primitive! (string->number r)
			       (string->number g)
			       (string->number b))
     (display "Set rgb\n")
     (values '((content-type . (text/plain)))
	     "rad\n"))
    (_ (values '((content-type . (text/plain))) "luser\n"))))

(run-server led-command-handler)
