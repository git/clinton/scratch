/* -*- c -*- */
/* Arduino rgb led driver */
/* Copyright (c) 2014 Clinton Ebadi <clinton@unknownlamer.org> */
/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#define DITHER_BITS 1
const int led_color_max = pow (2, 16) - 1;
const int dither_steps = pow (2, DITHER_BITS);

typedef struct rgb_pins
{
    int red;
    int green;
    int blue;
} rgb_pins;

const int rgb_banks = 2;

//const rgb_pins pins[rgb_banks] = { {10, 9, 11},
//                                   {5, 6, 3} };
const rgb_pins pins[rgb_banks] = { {9, 6, 10},
                                   {4, 5, 3} };

void set_color_x (int r, int g, int b, int dither_step = 0)
{
 int offset = dither_step * 256;

 for (int bank = 0; bank < rgb_banks; bank++)
 {
   analogWrite (pins[bank].red, r);
   analogWrite (pins[bank].green, g);
   analogWrite (pins[bank].blue, b);
 }
}

void setup ()
{
 Serial.begin (115200);
 Serial.setTimeout (20000);
 analogWriteResolution (16);
 randomSeed (analogRead (14));

 for (int bank = 0; bank < rgb_banks; bank++)
 {
   pinMode (pins[bank].red, OUTPUT);
   pinMode (pins[bank].green, OUTPUT);
   pinMode (pins[bank].blue, OUTPUT); 
 }

  // Test sequence to confirm wiring
  set_color_x (led_color_max, led_color_max, led_color_max);
  delay(500);
  set_color_x (led_color_max, 0, 0);
  delay (500);
  set_color_x (0, led_color_max, 0);
  delay (500);
  set_color_x (0, 0, led_color_max);
  delay (500);
}

void loop ()
{
  static int r = led_color_max / 2;
  static int g = led_color_max / 2;
  static int b = led_color_max / 2;
  static int step = -1;
  
  //step = (step + 1) % dither_steps;
  step = 0; // dithering is bogus/i'm-not-doing-it-right

  if (Serial.available ())
    {
      r = Serial.parseInt ();
      g = Serial.parseInt ();
      b = Serial.parseInt ();

      Serial.print ("set color ");
      Serial.print (r);
      Serial.print (" ");
      Serial.print (g);
      Serial.print (" ");
      Serial.print (b);
      Serial.println ();
      
      set_color_x (r, g, b);
    }

  // display random colors when the serial port is closed
  if (!Serial.dtr ())
    {
      set_color_x (random (led_color_max), random (led_color_max), random (led_color_max));
      delay (1500);
    }

  set_color_x (r, g, b, step);

}
