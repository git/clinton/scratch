(use-modules (srfi srfi-1))

(define distance (a b)
  (abs (- a b)))

(define (distance-change bpm_c bpm_n bpm_n+1)
  (- (abs (distance bpm_c bpm_n) (distance bpm_c bpm_n+1))
     (distance bpm_a bpm_b)))

(define (insert-song song playlist)
  (let ((last
	 (let find-minimum ((current-minimum (inf))
			    (min-cons '())
			    (plist playlist))
	   (cond
	    ((null? (cdr rest))
	     '())
	    ((< (distance-change song (first plist) (second plist))
		current-minimum)
	     (find-minimum (distance-change song (first plist) (second plist))
			   (first plist)
			   (rest plist)))
	    (else (find-minimum current-minimum min-cons (rest plist)))))))
    (let insert-copy ((old-list playlist))
      (cond ((eq? last old-list)
	     ;; (list... (car old-list) song (cdr old-list))
	     (cons* (car old-list) song (cdr old-list)))
	    (else (cons (car old-list) (insert-copy (cdr old-list))))))))

