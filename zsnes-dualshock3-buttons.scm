;;; Calculate ZSNES buttons for Dualshock controllers

;; From: http://blog.brokenfunction.com/2009/07/zsnes-and-a-sixaxis-controller/#comment-56667
;; Run zsnes and quit.
;; Look for the lines (something like)
;; Device 0 Sony PLAYSTATION(R)3 Controller
;; 27 axis, 19 buttons, 0 hats, 0 balls
;; Device 1 Sony PLAYSTATION(R)3 Controller
;; 27 axis, 19 buttons, 0 hats, 0 balls

;; Note the number of axes and buttons for controller 1 (first) and second (2).
;; e.g. axes1, buttons1, axes2, buttons2

;; Controller 1′s select button (base) will be 256 + (axes1*2)

;; Controller 2′s select button (base) will be 256 + (axes1*2) + buttons1 + (axes2*2)
;; Equivalently, it’s Controller 1′s base + buttons1 + (axes2*2)

;; Unfortunately, you can’t use 3 controllers this way, because it has
;; a hardcoded max offset of 448 (not sure why). It would have to be
;; recompiled at least to get around this but that might be based on a
;; dependency.

;; You can see the logic for this in zsnes’s source code, in src/linux/sdllink.c in
;; BOOL InitJoystickInput(void){}

(define axis-count 29)
(define button-count 17)

(define (base-button) (+ 256 (* 2 axis-count)))

(define (base-button-2)
  (+ (base-button) button-count (* 2 axis-count)))

(define (zsnes-buttons js-button)
  (values (+ (base-button) js-button)
	  (+ (base-button-2) js-button)))